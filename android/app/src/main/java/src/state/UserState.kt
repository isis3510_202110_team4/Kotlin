package src.state

import mvi.BaseState
import src.intent.UserIntent
import src.model.Usuario

data class UserState(   val otraView: Boolean= false,
                        val pedir: Boolean= false,
                        val proveer: Boolean=false,
                        val volver: Boolean=false,
                        val registrarCliente: Boolean=false,
                        val isLoading: Boolean = false,
                        val errorMessage: String? = null,
                        val registrarWorker: Boolean=false,
                        val login: Boolean=false,
                        val enterLogin: Boolean=false,
                        val goProfile: Boolean=false,
                        val allServices: Boolean=false,
                        val recommended: Boolean=false,
                        val plumbing: Boolean= false,
                        val electricity: Boolean=false,
                        val carpentry: Boolean=false,
                        val landscaping: Boolean=false,
                        val housekeeping: Boolean=false,
                        val other: Boolean=false,
                        val listaServicios : Boolean=false

) : BaseState