package src.state

import mvi.BaseState
import src.model.Service

data class ServiceState(
    val services : Boolean = false,
    val isLoading : Boolean = false,
    val errorMessage : String? = null,
): BaseState