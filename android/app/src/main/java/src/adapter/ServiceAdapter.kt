package src.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.example.services_app.databinding.ServiceListBinding
import src.model.Service

class ServiceAdapter : ListAdapter<Service, ServiceViewHolder> (ServiceAdapter){
    companion object : DiffUtil.ItemCallback<Service> (){
        override fun areItemsTheSame(oldItem: Service, newItem: Service): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Service, newItem: Service): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServiceViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ServiceListBinding.inflate(inflater, parent, false)
        return ServiceViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ServiceViewHolder, position: Int) {
        val binding = holder.binding as ServiceListBinding
        val currentService = getItem(position)
        binding.run {
            service = currentService
            executePendingBindings()
        }
    }
}

class ServiceViewHolder(val binding: ViewBinding) : RecyclerView.ViewHolder(binding.root)