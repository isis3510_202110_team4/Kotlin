package src.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.example.services_app.databinding.WorkerListBinding
import src.model.Worker
import src.view.WorkerProfile

class WorkerAdapter : ListAdapter<Worker, WorkerViewHolder> (WorkerAdapter){
    companion object : DiffUtil.ItemCallback<Worker>(){
        override fun areItemsTheSame(oldItem: Worker, newItem: Worker): Boolean {
            return oldItem === newItem
        }
        override fun areContentsTheSame(oldItem: Worker, newItem: Worker): Boolean {
            return oldItem == newItem
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkerViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = WorkerListBinding.inflate(inflater, parent, false)
        return WorkerViewHolder(binding)
    }
    override fun onBindViewHolder(holder: WorkerViewHolder, position: Int) {
        val binding = holder.binding as WorkerListBinding
        var currentWorker = getItem(position)
        binding.run {
            if (currentWorker.rating==null)
            {
                currentWorker.rating=5.0
            }
            worker = currentWorker
            executePendingBindings()
        }
        val selectWorker = View.OnClickListener {
            val intent = Intent(holder.itemView.context, WorkerProfile::class.java)
            intent.putExtra("username", currentWorker.username)
            intent.putExtra("name", currentWorker.name)
            intent.putExtra("email", currentWorker.username)
            intent.putExtra("number", currentWorker.phone_number)
            intent.putExtra("id", currentWorker.id_number)
            intent.putExtra("ratio", currentWorker.service_radius.toString())
            intent.putExtra("category", currentWorker.category?.get(0))
            intent.putExtra("rating", currentWorker.rating.toString())
            intent.putExtra("price", currentWorker.price_per_hour.toString())
            intent.putExtra("solicitate",true)
            intent.putExtra("id_db", currentWorker.id)
            intent.putExtra("profile_picture", currentWorker.profile_picture)
            holder.itemView.context.startActivity(intent)
        }
        holder.itemView.setOnClickListener(selectWorker)
    }
}
class WorkerViewHolder(val binding: ViewBinding) : RecyclerView.ViewHolder(binding.root)