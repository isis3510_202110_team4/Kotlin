package src.back
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.awaitResponse
import src.model.*
import java.util.*

class RestApiService {
    suspend fun addUser(userData: Usuario, onResult: (Usuario?) -> Unit): Call<Usuario> {
        val retrofit = ServiceBuilder.buildService(UsersApi::class.java)
        var a=retrofit.registerUser(userData)
        return a
    }
    suspend fun loginUser(userData: Usuario, onResult: (Usuario?) -> Unit): Call<Usuario> {
        val retrofit = ServiceBuilder.buildService(UsersApi::class.java)
        var a=retrofit.loginUser(userData)
        return a
    }
    suspend fun addWorker(userData: Worker, onResult: (Worker?) -> Unit): Call<Worker> {
        val retrofit = ServiceBuilder.buildService(WorkerApi::class.java)
        var a=retrofit.registerWorker(userData)
        return a
    }
    suspend fun getUsers( onResult: (Usuario?) -> Unit): Call<List<Usuario>> {
        val retrofit = ServiceBuilder.buildService(UsersApi::class.java)
        var a=retrofit.getUsers()
        return a
    }
    suspend fun getWorkers( onResult: (Worker?) -> Unit): List<Worker> {
        val retrofit = ServiceBuilder.buildService(WorkerApi::class.java)
        var a=retrofit.getWorkers()
        return a
    }
    suspend fun getFavoriteCategory( id: String, onResult: (Locale.Category?) -> Unit): Call<Categoria> {
        val retrofit = ServiceBuilder.buildService(UsersApi::class.java)
        var a=retrofit.getFavoriteCategory(id)
        return a
    }
    suspend fun getBestByCategory( onResult: (Locale.Category?) -> Unit): Call<List<Worker>> {
        val retrofit = ServiceBuilder.buildService(UsersApi::class.java)
        var a=retrofit.getBestWorkerPerCategory()
        return a
    }
    suspend fun createService(userData: Service, onResult: (Service?) -> Unit): Call<Service> {
        val retrofit = ServiceBuilder.buildService(UsersApi::class.java)
        var a=retrofit.createService(userData)
        return a
    }
    suspend fun getServices(id:String, onResult: (Service?) -> Unit) : List<Service> {
        val retrofit = ServiceBuilder.buildService(ServiceApi::class.java)
        var a = retrofit.getServices(id)
        return a
    }
    suspend fun getAllServices(onResult: (Service?) -> Unit) : List<Service> {
        val retrofit = ServiceBuilder.buildService(ServiceApi::class.java)
        var a = retrofit.getAllServices()
        return a
    }
}