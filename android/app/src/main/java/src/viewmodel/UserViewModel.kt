package src.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch
import mvi.BaseModel
import src.intent.UserIntent
import src.state.UserState
import java.lang.Exception

class UserViewModel() : ViewModel(), BaseModel<UserIntent, UserState> {

    override val intents: Channel<UserIntent> = Channel(Channel.UNLIMITED)
    private val _state = MutableLiveData<UserState>().apply { value = UserState() }
    override val state: LiveData<UserState>
        get() = _state
    init    {
        handlerIntent()
    }
    private fun handlerIntent(){
        viewModelScope.launch (Dispatchers.Main){
            intents.consumeAsFlow().collect {
                    workerIntent -> when(workerIntent){
                UserIntent.EnterApp -> enterApp()
                UserIntent.volver -> volver()
                UserIntent.prestar -> enterProveer()
                UserIntent.solicitar -> enterPedir()
                UserIntent.registrar -> registrar()
                UserIntent.login -> login()
                UserIntent.enterLogin -> enterLogin()
                UserIntent.registrarWorker-> registrarWorker()
                UserIntent.goProfile-> goProfile()
                UserIntent.allServices-> allServices()
                UserIntent.recommended -> getRecommended()
                UserIntent.Plumbing -> plumbing()
                UserIntent.Electricity -> electricity()
                UserIntent.Carpentry -> carpentry()
                UserIntent.Landscaping -> landscaping()
                UserIntent.Housekeeping -> houskeeping()
                UserIntent.Other -> other()
                UserIntent.ListaServicios -> listaServicios()
            }
            }
        }
    }
    private fun enterLogin(){
        viewModelScope.launch(Dispatchers.IO) {
            try {
                updateState { it.copy(isLoading = true) }
                allFalse()
                updateState { it.copy( enterLogin = true)}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }
    private fun login(){
        viewModelScope.launch(Dispatchers.IO) {
            try {
                updateState { it.copy(isLoading = true) }
                allFalse()
                updateState { it.copy( login = true)}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }
    private fun registrar(){
        viewModelScope.launch(Dispatchers.IO) {
            try {
                updateState { it.copy(isLoading = true) }
                allFalse()
                updateState { it.copy(registrarCliente = true)}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }
    private fun registrarWorker(){
        viewModelScope.launch(Dispatchers.IO) {
            try {
                updateState { it.copy(isLoading = true) }
                updateState { it.copy(
                    isLoading = false,
                    pedir = false,
                    proveer = false,
                    volver= false,
                    registrarCliente = false,
                    otraView = false,
                    login = false,
                    enterLogin = false,
                    registrarWorker = true,
                    allServices=false,
                    goProfile = false ,
                    plumbing = false,
                    electricity = false,
                    carpentry = false,
                    landscaping = false,
                    housekeeping = false,
                    other = false,
                    listaServicios = false
                )}

            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }
    private fun volver(){
        viewModelScope.launch(Dispatchers.IO) {
            try {
                updateState { it.copy(isLoading = true) }
                updateState { it.copy(
                    isLoading = false,
                    pedir = false,
                    proveer = false,
                    volver= true,
                    registrarCliente = false,
                    otraView = false,
                    login = false,
                    enterLogin = false,
                    registrarWorker = false,
                    allServices=false,
                    goProfile = false ,
                    plumbing = false,
                    electricity = false,
                    carpentry = false,
                    landscaping = false,
                    housekeeping = false,
                    other = false,
                    listaServicios = false
                )}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }
    private fun enterProveer(){
        viewModelScope.launch(Dispatchers.IO) {
            try {
                updateState { it.copy(isLoading = true) }
                allFalse()
                updateState { it.copy(proveer = true)}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }

    private fun enterPedir(){
        viewModelScope.launch(Dispatchers.IO) {
            try {
                updateState { it.copy(isLoading = true) }
                allFalse()
                updateState { it.copy(pedir = true)}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }

    private fun enterApp(){
        viewModelScope.launch(Dispatchers.IO) {
            try {
                updateState { it.copy(isLoading = true) }
                allFalse()
                updateState { it.copy(otraView = true)}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }

    private suspend fun goProfile(){

        viewModelScope.launch(Dispatchers.Main) {
            allFalse()
            try {
                updateState { it.copy(isLoading = true) }
                updateState { it.copy(
                    isLoading = false,
                    pedir = false,
                    proveer = false,
                    volver= false,
                    registrarCliente = false,
                    otraView = false,
                    login = false,
                    enterLogin = false,
                    registrarWorker = false,
                    allServices=false,
                    goProfile = true ,
                    plumbing = false,
                    electricity = false,
                    carpentry = false,
                    landscaping = false,
                    housekeeping = false,
                    other = false,
                    listaServicios = false
                )}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }
    private suspend fun allServices(){

        viewModelScope.launch(Dispatchers.Main) {
            try {
                updateState { it.copy(isLoading = true) }
                updateState { it.copy(
                    isLoading = false,
                    pedir = false,
                    proveer = false,
                    volver= false,
                    registrarCliente = false,
                    otraView = false,
                    login = false,
                    enterLogin = false,
                    registrarWorker = false,
                    allServices=true,
                    goProfile = false ,
                    plumbing = false,
                    electricity = false,
                    carpentry = false,
                    landscaping = false,
                    housekeeping = false,
                    other = false,
                    listaServicios = false
                )}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }
    private suspend fun plumbing(){

        viewModelScope.launch(Dispatchers.Main) {
            try {
                updateState { it.copy(isLoading = true) }
                updateState { it.copy(
                    isLoading = false,
                    pedir = false,
                    proveer = false,
                    volver= false,
                    registrarCliente = false,
                    otraView = false,
                    login = false,
                    enterLogin = false,
                    registrarWorker = false,
                    allServices=false,
                    goProfile = false ,
                    plumbing = true,
                    electricity = false,
                    carpentry = false,
                    landscaping = false,
                    housekeeping = false,
                    other = false,
                    listaServicios = false
                )}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }
    private suspend fun electricity(){

        viewModelScope.launch(Dispatchers.Main) {
            try {
                updateState { it.copy(isLoading = true) }
                updateState { it.copy(
                    isLoading = false,
                    pedir = false,
                    proveer = false,
                    volver= false,
                    registrarCliente = false,
                    otraView = false,
                    login = false,
                    enterLogin = false,
                    registrarWorker = false,
                    allServices=false,
                    goProfile = false ,
                    plumbing = false,
                    electricity = true,
                    carpentry = false,
                    landscaping = false,
                    housekeeping = false,
                    other = false,
                    listaServicios = false
                )}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }
    private suspend fun carpentry(){

        viewModelScope.launch(Dispatchers.Main) {
            try {
                updateState { it.copy(isLoading = true) }
                updateState { it.copy(
                    isLoading = false,
                    pedir = false,
                    proveer = false,
                    volver= false,
                    registrarCliente = false,
                    otraView = false,
                    login = false,
                    enterLogin = false,
                    registrarWorker = false,
                    allServices=false,
                    goProfile = false ,
                    plumbing = false,
                    electricity = false,
                    carpentry = true,
                    landscaping = false,
                    housekeeping = false,
                    other = false,
                    listaServicios = false
                )}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }
    private suspend fun landscaping(){

        viewModelScope.launch(Dispatchers.Main) {
            try {
                updateState { it.copy(isLoading = true) }
                updateState { it.copy(
                    isLoading = false,
                    pedir = false,
                    proveer = false,
                    volver= false,
                    registrarCliente = false,
                    otraView = false,
                    login = false,
                    enterLogin = false,
                    registrarWorker = false,
                    allServices=false,
                    goProfile = false ,
                    plumbing = false,
                    electricity = false,
                    carpentry = false,
                    landscaping = true,
                    housekeeping = false,
                    other = false,
                    listaServicios = false
                )}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }
    private suspend fun houskeeping(){

        viewModelScope.launch(Dispatchers.Main) {
            try {
                updateState { it.copy(isLoading = true) }
                updateState { it.copy(
                    isLoading = false,
                    pedir = false,
                    proveer = false,
                    volver= false,
                    registrarCliente = false,
                    otraView = false,
                    login = false,
                    enterLogin = false,
                    registrarWorker = false,
                    allServices=false,
                    goProfile = false ,
                    plumbing = false,
                    electricity = false,
                    carpentry = false,
                    landscaping = false,
                    housekeeping = true,
                    other = false,
                    listaServicios = false
                )}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }
    private suspend fun other(){

        viewModelScope.launch(Dispatchers.Main) {
            try {
                updateState { it.copy(isLoading = true) }
                updateState { it.copy(
                    isLoading = false,
                    pedir = false,
                    proveer = false,
                    volver= false,
                    registrarCliente = false,
                    otraView = false,
                    login = false,
                    enterLogin = false,
                    registrarWorker = false,
                    allServices=false,
                    goProfile = false ,
                    plumbing = false,
                    electricity = false,
                    carpentry = false,
                    landscaping = false,
                    housekeeping = false,
                    other = true,
                    recommended = false,
                    listaServicios = false
                )}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }

    private suspend fun listaServicios(){

        viewModelScope.launch(Dispatchers.Main) {
            try {
                updateState { it.copy(isLoading = true) }
                updateState { it.copy(
                    isLoading = false,
                    pedir = false,
                    proveer = false,
                    volver= false,
                    registrarCliente = false,
                    otraView = false,
                    login = false,
                    enterLogin = false,
                    registrarWorker = false,
                    allServices=false,
                    goProfile = false ,
                    plumbing = false,
                    electricity = false,
                    carpentry = false,
                    landscaping = false,
                    housekeeping = false,
                    other = false,
                    recommended = false,
                    listaServicios = true
                )}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }

   private suspend fun allFalse(){
                updateState { it.copy(isLoading = false, pedir = false,proveer = false,volver= false,registrarCliente = false,otraView = false, login = false,enterLogin = false, registrarWorker = false, goProfile = false,allServices=false)}
    }


    private suspend fun getRecommended(){
        viewModelScope.launch(Dispatchers.Main) {
            try {
                updateState { it.copy(isLoading = true) }
                updateState { it.copy(
                    isLoading = false,
                    pedir = false,
                    proveer = false,
                    volver= false,
                    registrarCliente = false,
                    otraView = false,
                    login = false,
                    enterLogin = false,
                    registrarWorker = false,
                    allServices=false,
                    goProfile = false ,
                    plumbing = false,
                    electricity = false,
                    carpentry = false,
                    landscaping = false,
                    housekeeping = false,
                    other = false,
                    recommended = true
                )}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }
        }
    }

    private suspend fun updateState(handler: suspend (intent: UserState) -> UserState){
        _state.postValue(handler(state.value!!))
    }

}