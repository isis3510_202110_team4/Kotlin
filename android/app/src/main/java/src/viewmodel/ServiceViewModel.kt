package src.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch
import mvi.BaseModel
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.GlobalContext
import retrofit2.awaitResponse
import src.back.RestApiService
import src.intent.ServiceIntent
import src.intent.WorkerIntent
import src.state.ServiceState
import src.state.WorkerState

class ServiceViewModel() : ViewModel(), BaseModel<ServiceIntent, ServiceState>{
    override val intents: Channel<ServiceIntent> = Channel(Channel.UNLIMITED)
    private val _state = MutableLiveData<ServiceState>().apply { value = ServiceState() }
    override val state: LiveData<ServiceState>
        get() = _state
    init {
        handlerIntent()
    }

    private fun handlerIntent(){
        viewModelScope.launch {
            intents.consumeAsFlow().collect {
                    workerIntent -> when(workerIntent){
                ServiceIntent.RefreshServices -> fetchData()
                ServiceIntent.FetchServices -> fetchData()
                ServiceIntent.allFalse -> allFalse()
            }
            }
        }
    }


    private fun fetchData(){
        viewModelScope.launch(Dispatchers.IO) {
            try {
                updateState { it.copy(isLoading = true) }
                updateState { it.copy(isLoading = false, services = true)}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }


        }
    }
    private fun allFalse(){
        viewModelScope.launch(Dispatchers.IO) {
            try {
                updateState { it.copy(isLoading = false, services = false)}
            }
            catch (e : Exception){
                e.printStackTrace()
                updateState { it.copy(isLoading = false, errorMessage = e.message) }
            }


        }
    }

    private suspend fun updateState(handler: suspend (intent : ServiceState) -> ServiceState){
        _state.postValue(handler(state.value!!))
    }
}