package src.view

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import com.example.services_app.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.awaitResponse
import src.back.RestApiService
import src.model.Usuario
import src.model.Worker

class Profile : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        var user:TextView  = findViewById(R.id.usernameS)
        var name:TextView  = findViewById(R.id.nameS)
        var email:TextView  = findViewById(R.id.emailS)
        var number:TextView  = findViewById(R.id.numberS)
        var id:TextView  = findViewById(R.id.idS)
        val sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE)
        var  u= sharedPreferences.getString("username",null);
        var  n= sharedPreferences.getString("name",null);
        var  e= sharedPreferences.getString("email",null);
        var  nu= sharedPreferences.getString("number",null);
        var  i= sharedPreferences.getString("id",null);
        if (n != null&&u != null&&e != null&&nu != null&&i != null) {
            user.setText(u)
            name.setText(n)
            email.setText(e)
            number.setText(nu)
            id.setText(i)
        }

    }


}