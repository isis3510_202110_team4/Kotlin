package src.view

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.AdapterView.OnItemClickListener
import android.widget.Button
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.services_app.R
import kotlinx.coroutines.launch
import mvi.BaseView
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.awaitResponse
import src.adapter.WorkerAdapter
import src.back.RestApiService
import src.intent.UserIntent
import src.state.UserState
import src.viewmodel.UserViewModel
import androidx.lifecycle.Observer
import kotlinx.coroutines.Dispatchers

class ListaServicios : AppCompatActivity(), BaseView<UserState> {
    private val mViewModel by viewModel<UserViewModel>()
    private val helper= Helper()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_servicios)
        mViewModel.state.observe(this, Observer {
            render(it)
        })
        var boton:Button  = findViewById(R.id.buttonProfile)
        var boton2 : Button = findViewById(R.id.buttonRecommended)
        var boton3 : Button = findViewById(R.id.buttonServices)
        boton.setOnClickListener {
            if (helper.isConnected(this))
            {
                lifecycleScope.launch {
                    mViewModel.intents.send(UserIntent.goProfile)
                }
            }
            else{
                helper.showNoConnectionDialog(this)
            }
        }

        boton2.setOnClickListener {
            if (helper.isConnected(this)) {
                lifecycleScope.launch {
                    mViewModel.intents.send(UserIntent.recommended)

                }
            } else {
                helper.showNoConnectionDialog(this)
            }
        }

        boton3.setOnClickListener {
            if (helper.isConnected(this)) {
                lifecycleScope.launch {
                    mViewModel.intents.send(UserIntent.ListaServicios)

                }
            } else {
                helper.showNoConnectionDialog(this)
            }
        }

        val itemClickListener =
            OnItemClickListener { listView, v, position, id ->

                if (helper.isConnected(this))
                {
                    if (position == 1) {
                        lifecycleScope.launch {
                            mViewModel.intents.send(UserIntent.allServices)
                        }
                    }
                    if  (position == 2) {
                        lifecycleScope.launch {
                            mViewModel.intents.send(UserIntent.Plumbing)
                        }
                    }
                    if  (position == 3) {
                        lifecycleScope.launch {
                            mViewModel.intents.send(UserIntent.Electricity)
                        }
                    }
                    if  (position == 4) {
                        lifecycleScope.launch {
                            mViewModel.intents.send(UserIntent.Carpentry)
                        }
                    }
                    if  (position == 5) {
                        lifecycleScope.launch {
                            mViewModel.intents.send(UserIntent.Landscaping)
                        }
                    }
                    if  (position == 6) {
                        lifecycleScope.launch {
                            mViewModel.intents.send(UserIntent.Housekeeping)
                        }
                    }
                    if  (position == 7) {
                        lifecycleScope.launch {
                            mViewModel.intents.send(UserIntent.Other)
                        }
                    }
                }
                else{
                    helper.showNoConnectionDialog(this)
                }
            }
        val listView: ListView = findViewById<View>(R.id.list_options) as ListView
        listView.setOnItemClickListener(itemClickListener)
        val textView = TextView(this@ListaServicios)
        textView.text = "What service are you looking for today?"
        listView.addHeaderView(textView)
        textView.setTextSize(1, 19.0F)
    }
fun workerProfile(trabajador:Boolean)
{
    val intent = Intent(
            this@ListaServicios,
            WorkerProfile::class.java
    )
    startActivity(intent)
}
    fun userProfile(trabajador:Boolean)
    {
        val intent = Intent(
                this@ListaServicios,
                Profile::class.java
        )
        startActivity(intent)
    }
    override fun render(state: UserState) {
        with(state) {
            when {
                state.goProfile -> {
                    val sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE)
                    var trabajador=sharedPreferences.getBoolean("is",false)
                    if (trabajador)
                    {
                        workerProfile(trabajador)
                    }
                    else
                    {
                        userProfile(trabajador)
                    }

                }
                state.allServices -> {
                    val intent = Intent(
                        this@ListaServicios,
                        MainActivity::class.java
                    )
                    intent.putExtra("myId", getIntent().getStringExtra("myId"))
                    intent.putExtra("filter", "no")
                    startActivity(intent)

                }
                state.allServices -> {
                    val intent = Intent(
                        this@ListaServicios,
                        MainActivity::class.java
                    )
                    intent.putExtra("myId", getIntent().getStringExtra("myId"))
                    intent.putExtra("filter", "no")
                    startActivity(intent)

                }
                state.plumbing -> {
                    val intent = Intent(
                        this@ListaServicios,
                        MainActivity::class.java
                    )
                    intent.putExtra("myId", getIntent().getStringExtra("myId"))
                    intent.putExtra("filter", "Plumbing")
                    startActivity(intent)

                }
                state.electricity -> {
                    val intent = Intent(
                        this@ListaServicios,
                        MainActivity::class.java
                    )
                    intent.putExtra("myId", getIntent().getStringExtra("myId"))
                    intent.putExtra("filter", "Electricity")
                    startActivity(intent)

                }
                state.carpentry -> {
                    val intent = Intent(
                        this@ListaServicios,
                        MainActivity::class.java
                    )
                    intent.putExtra("myId", getIntent().getStringExtra("myId"))
                    intent.putExtra("filter", "Carpentry")
                    startActivity(intent)

                }
                state.landscaping -> {
                    val intent = Intent(
                        this@ListaServicios,
                        MainActivity::class.java
                    )
                    intent.putExtra("myId", getIntent().getStringExtra("myId"))
                    intent.putExtra("filter", "Landscaping")
                    startActivity(intent)

                }
                state.housekeeping -> {
                    val intent = Intent(
                        this@ListaServicios,
                        MainActivity::class.java
                    )
                    intent.putExtra("myId", getIntent().getStringExtra("myId"))
                    intent.putExtra("filter", "Housekeeping")
                    startActivity(intent)

                }
                state.other -> {
                    val intent = Intent(
                        this@ListaServicios,
                        MainActivity::class.java
                    )
                    intent.putExtra("myId", getIntent().getStringExtra("myId"))
                    intent.putExtra("filter", "Other")
                    startActivity(intent)

                }
                state.listaServicios -> {
                    val intent = Intent(
                        this@ListaServicios,
                        ServiceListActivity::class.java
                    )
                    intent.putExtra("myId", getIntent().getStringExtra("myId"))
                    startActivity(intent)
                }
                state.recommended -> {

                    val intent = Intent(this@ListaServicios, WorkerProfile::class.java)
                    val api = RestApiService()
                    val sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE)
                    val idUser = sharedPreferences.getString("myId", null)
                    if(idUser != null){
                        lifecycleScope.launch(Dispatchers.Main) {
                            val response = api.getFavoriteCategory(idUser){

                            }

                            val message = response.awaitResponse()
                            val errors = message.code()
                            if(message.isSuccessful){
                                val response2 = api.getBestByCategory(){

                                }
                                val message2 = response2.awaitResponse()
                                if(message2.isSuccessful){
                                    if(message2.body()?.isNullOrEmpty()==false){
                                        message2.body()?.forEach {
                                            val currentWorker = it
                                            it.category?.forEach {
                                                if(message.body()?.name!=null && it == message.body()?.name){

                                                    intent.putExtra("username", currentWorker.username)
                                                    intent.putExtra("name", currentWorker.name)
                                                    intent.putExtra("email", currentWorker.email)
                                                    intent.putExtra("number", currentWorker.phone_number)
                                                    intent.putExtra("id", currentWorker.id_number.toString())
                                                    intent.putExtra("is", true)
                                                    intent.putExtra("rating", currentWorker.rating.toString())
                                                    intent.putExtra("price", currentWorker.price_per_hour.toString())
                                                    intent.putExtra("category",
                                                        currentWorker.category?.get(0)
                                                    )
                                                    intent.putExtra("ratio", currentWorker.service_radius.toString())
                                                    intent.putExtra("solicitate", true)
                                                    startActivity(intent)
                                                }
                                            }
                                        }
                                    }
                                    else{
                                        Toast.makeText(this@ListaServicios, "Sorry, seems like there are no recommendations for you yet", Toast.LENGTH_SHORT).show()
                                    }
                                }
                            }
                            else{
                                print(errors)
                                print("no tan primero del mundo")
                            }
                        }

                    }


                }
            }
            if (errorMessage != null) {
                Toast.makeText(this@ListaServicios, errorMessage, Toast.LENGTH_SHORT).show()
            }

    }
    }
}