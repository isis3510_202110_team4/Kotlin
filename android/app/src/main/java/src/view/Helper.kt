package src.view

import android.app.Activity
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AlertDialog
import com.example.services_app.R

 class Helper {
    fun showNoConnectionDialog(activity: Activity) {
        val alertDialog = AlertDialog.Builder(activity)
        alertDialog.apply {
            setTitle("No Connection")
            setIcon(R.drawable.baseline_cloud_off_black_24dp)
            setMessage("Your network connection is unavailable. Check your data or wifi connection and try again.")

        }.create().show()
    }
    fun showWrongLoginDialog(activity: Activity) {
        val alertDialog = AlertDialog.Builder(activity)
        alertDialog.apply {
            setTitle("Wrong login")
            setIcon(R.drawable.outline_report_problem_black_24)
            setMessage("You have entered an incorrect username or password")
        }.create().show()
    }
    fun showWrongRegisterDialog(error: String,activity: Activity) {
        val alertDialog = AlertDialog.Builder(activity)
        alertDialog.apply {
            setTitle("Wrong Register")
            setIcon(R.drawable.outline_report_problem_black_24)
            setMessage(error)
        }.create().show()
    }
     fun showErrorDialog(title:String,error: String,activity: Activity) {
         val alertDialog = AlertDialog.Builder(activity)
         alertDialog.apply {
             setTitle(title)
             setIcon(R.drawable.outline_report_problem_black_24)
             setMessage(error)
         }.create().show()
     }

    fun isConnected(actividad: Activity): Boolean {
        val manager: ConnectivityManager = actividad.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val wifi: NetworkInfo? = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
        val mobile: NetworkInfo? = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
        return (wifi!=null && wifi.isConnected)||(mobile!=null && mobile.isConnected)
    }

 }