package src.view

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.launch
import mvi.BaseView
import org.koin.androidx.viewmodel.ext.android.viewModel
import src.adapter.ServiceAdapter
import src.intent.ServiceIntent
import src.viewmodel.ServiceViewModel
import src.state.ServiceState
import com.example.services_app.R
import okhttp3.internal.wait
import org.koin.core.component.getScopeId
import retrofit2.awaitResponse
import src.back.RestApiService
import src.model.Service
import src.model.Worker
import java.lang.Thread.sleep

class ServiceListActivity: AppCompatActivity(), BaseView<ServiceState> {

    private val mAdapter = ServiceAdapter()
    private val mViewModel by viewModel<ServiceViewModel>()
    private val helper = Helper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.services_activity)
        recyclerView.adapter = mAdapter
        mViewModel.state.observe(this, Observer {
            render(it)
        })

        lifecycleScope.launch {
            if(helper.isConnected(this@ServiceListActivity)){
                mViewModel.intents.send(ServiceIntent.FetchServices)
            }
            else{
                helper.showNoConnectionDialog(this@ServiceListActivity)
            }
        }
    }
    override fun render(state: ServiceState) {
        with(state){
            progressBar.isVisible = isLoading
            progressBar.isEnabled = !isLoading
            if(services){
                lifecycleScope.launch {
                    mViewModel.intents.send(ServiceIntent.allFalse)
                    val service = RestApiService()
                    val sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE)
                    val id = sharedPreferences.getString("myId","1")
                    val list = service.getServices(id!!){}
                    var newServices = mutableListOf<Service>()
                    if (!list.isEmpty())
                    {
                        for(i in list){
                            if (i.rating==null)
                            {
                                i.rating= 5.0.toString()
                            }
                            newServices.add(i)
                        }
                    }else
                    {
                        mViewModel.intents.send(ServiceIntent.allFalse)
                      helper.showErrorDialog("No Services Yet","It seems like you have not ask for any services yet", this@ServiceListActivity)

                    }


                    mAdapter.submitList(newServices)
                    if(errorMessage!=null){
                            if(helper.isConnected(this@ServiceListActivity))
                            {
                                Toast.makeText(this@ServiceListActivity, errorMessage, Toast.LENGTH_SHORT).show()
                            }
                            else{
                                helper.showNoConnectionDialog(this@ServiceListActivity)
                            }
                        }
                    }
                }

            }

        }
    }
