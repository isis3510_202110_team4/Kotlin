package src.view

import android.Manifest
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.text.isDigitsOnly
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.viewbinding.ViewBinding
import com.example.services_app.R
import com.example.services_app.databinding.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.completed.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mvi.BaseView
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.awaitResponse
import src.adapter.WorkerAdapter
import src.back.RestApiService
import src.intent.UserIntent
import src.model.Usuario
import src.model.Worker
import src.state.UserState
import src.viewmodel.UserViewModel
import java.lang.Exception
import java.util.*


class Register : AppCompatActivity(), BaseView<UserState> {

    private val mViewModel by viewModel<UserViewModel>()
    private val helper= Helper()
    private lateinit var bindingInicial: ActivityRegisterBinding
    private lateinit var workerBinder: WorkersSignUpBinding
    private lateinit var clientBinder: ClientsSignUpBinding
    private lateinit var completado: CompletedBinding
    private lateinit var fin: ActivityMainBinding
    private lateinit var loginBind: ActivitySignInBinding
    private val pasos = arrayOf("Basic Info", "Specifics", "Complete")
    var actual =0

    private lateinit var birthdate: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindingInicial = ActivityRegisterBinding.inflate(layoutInflater)
        workerBinder = WorkersSignUpBinding.inflate(layoutInflater)
        clientBinder = ClientsSignUpBinding.inflate(layoutInflater)
        completado = CompletedBinding.inflate(layoutInflater)
        fin = ActivityMainBinding.inflate(layoutInflater)
        loginBind = ActivitySignInBinding.inflate(layoutInflater)

        val view = bindingInicial.root
        setContentView(view)

        mViewModel.state.observe(this, Observer {
            render(it)
        })
        birthdate= findViewById<EditText>(R.id.birthDate)
        birthdate.setOnClickListener {
            dateSelector()
        }
        bindingInicial.Pasos.setLabels(pasos).setBarColorIndicator(Color.BLACK).setLabelColorIndicator(Color.parseColor("#0ca450")).setCompletedPosition(actual).drawView()
        bindingInicial.Pasos.completedPosition = actual

        bindingInicial.buttonPrestar.setOnClickListener {
            if (!todoLleno1()||!datosValidos1())
            {

            }
            else
            {
                avanzarPantalla(workerBinder, true)
                workerBinder.Pasos.setLabels(pasos).setBarColorIndicator(Color.BLACK).setLabelColorIndicator(Color.parseColor("#0ca450")).setCompletedPosition(actual).drawView()
                workerBinder.Pasos.completedPosition = actual
            }
        }
        bindingInicial.buttonSolicitar.setOnClickListener {
            if (!todoLleno1()||!datosValidos1())
            {

            }
            else
            {
                avanzarPantalla(clientBinder, true)
                clientBinder.Pasos.setLabels(pasos).setBarColorIndicator(Color.BLACK).setLabelColorIndicator(Color.parseColor("#0ca450")).setCompletedPosition(actual).drawView()
                clientBinder.Pasos.completedPosition = actual
            }
        }
        workerBinder.volver.setOnClickListener {
            lifecycleScope.launch {
                mViewModel.intents.send(UserIntent.volver)
            }
        }
        clientBinder.volver.setOnClickListener {
            lifecycleScope.launch {
                mViewModel.intents.send(UserIntent.volver)
            }
        }
        bindingInicial.buttonSignIn.setOnClickListener {
            avanzarPantalla(loginBind, true)

        }
        workerBinder.finalizar.setOnClickListener {
            if (!todoLleno2()||!datosValidos2())
            {

            }
            else {
                lifecycleScope.launch {

                    mViewModel.intents.send(UserIntent.registrarWorker)

                }
            }
        }
        clientBinder.finalizar.setOnClickListener {
            if (!todoLleno3()||!datosValidos3())
            {

            }
            else {
                lifecycleScope.launch(Dispatchers.IO) {
                    mViewModel.intents.send(UserIntent.registrar)

                }

            }}
        completado.Continue.setOnClickListener {
            lifecycleScope.launch {
                mViewModel.intents.send(UserIntent.EnterApp)
            }
        }
        loginBind.buttonSign.setOnClickListener {
            if (!todoLleno4())
            {

            }
            else{
                lifecycleScope.launch {
                    mViewModel.intents.send(UserIntent.login)
                }
            }
        }




    }
    fun isValidEmail(target: CharSequence?): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }


    fun datosValidos1(): Boolean {
        if (!isValidEmail(bindingInicial.editTextTextEmailAddress.text.trim()))
        {
            bindingInicial.editTextTextEmailAddress.setError("debe ingresar una direccion de correo valida")
            return false
        }
        return true
    }
    fun datosValidos2(): Boolean {
        if (!TextUtils.isDigitsOnly(workerBinder.Telefono.text.toString()))
        {
            workerBinder.Telefono.setError("debe ingresar un numero de telefono valido")
        }
        if (!TextUtils.isDigitsOnly(workerBinder.cedula.text.toString()))
        {
            workerBinder.cedula.setError("debe ingresar un numero de identificacion valido")
        }
        if (!TextUtils.isDigitsOnly(workerBinder.radio.text.toString()))
        {
            workerBinder.radio.setError("debe ingresar un radio de servicio valido")
        }

        if (!TextUtils.isDigitsOnly(workerBinder.precio.text.toString()))
        {
            workerBinder.precio.setError("debe ingresar un precio por hora valido")
        }

        return true
    }
    fun datosValidos3(): Boolean {
        if (!TextUtils.isDigitsOnly(clientBinder.Telefono.text.toString()))
        {
            clientBinder.Telefono.setError("debe ingresar un numero de telefono valido")
        }
        if (!TextUtils.isDigitsOnly(clientBinder.cedula.text.toString()))
        {
            clientBinder.cedula.setError("debe ingresar un numero de identificacion valido")
        }
        return true
    }
    fun todoLleno1(): Boolean {
        if (bindingInicial.editTextTextEmailAddress.text.trim().isEmpty())
        {
            bindingInicial.editTextTextEmailAddress.setError("debe completar este campo")
            return false
        }
        if (bindingInicial.editTextTextPassword.text.trim().isEmpty())
        {
            bindingInicial.editTextTextPassword.setError("debe completar este campo")
            return false
        }
        if (bindingInicial.editTextTextPersonName2.text.trim().isEmpty())
        {
            bindingInicial.editTextTextPersonName2.setError("debe completar este campo")
            return false
        }
        if (bindingInicial.editTextTextPassword2.text.trim().isEmpty())
        {
            bindingInicial.editTextTextPassword2.setError("debe completar este campo")
            return false
        }

        if (bindingInicial.editTextTextPassword2.text.trim() != bindingInicial.editTextTextPassword.text.trim())
        {
            bindingInicial.editTextTextPassword2.setError("las contraseñas no son iguales")
            return false
        }
        if (bindingInicial.birthDate.text.trim().isEmpty())
        {
            bindingInicial.birthDate.setError("debe completar este campo")
            return false
        }
        if (bindingInicial.birthDate.text.trim().isEmpty())
        {
            bindingInicial.birthDate.setError("debe completar este campo")
            return false
        }
        return true
    }
    fun todoLleno2(): Boolean {
        if (workerBinder.Direccion.text.trim().isEmpty())
        {
            workerBinder.Direccion.setError("debe completar este campo")
            return false
        }
        if (workerBinder.cedula.text.trim().isEmpty())
        {
            workerBinder.cedula.setError("debe completar este campo")
            return false
        }
        if (workerBinder.precio.text.trim().isEmpty())
        {
            workerBinder.precio.setError("debe completar este campo")
            return false
        }
        if (workerBinder.radio.text.trim().isEmpty())
        {
            workerBinder.radio.setError("debe completar este campo")
            return false
        }
        if (workerBinder.categoria.selectedItem.toString().equals("SERVICES"))
        {
            val toast = Toast.makeText(applicationContext, "You must select a service", Toast.LENGTH_SHORT)
            toast.show()
            return false
        }
        return true
    }
    fun todoLleno3(): Boolean {
        if (clientBinder.Direccion.text.trim().isEmpty())
        {
            clientBinder.Direccion.setError("debe completar este campo")
            return false
        }
        if (bindingInicial.editTextTextPersonName2.text.trim().isEmpty())
        {
            bindingInicial.editTextTextPersonName2.setError("debe completar este campo")
            return false
        }
        if (clientBinder.cedula.text.trim().isEmpty())
        {
            clientBinder.cedula.setError("debe completar este campo")
            return false
        }
        return true
    }
    fun todoLleno4(): Boolean {
        if (loginBind.editTextTextEmailAddress.text.trim().isEmpty())
        {
            loginBind.editTextTextEmailAddress.setError("debe completar este campo")
            return false
        }
        if (loginBind.editTextTextPassword.text.trim().isEmpty())
        {
            loginBind.editTextTextPassword.setError("debe completar este campo")
            return false
        }
        return true
    }

    fun avanzarPantalla(bind: ViewBinding?, avanza: Boolean) {
        if(avanza)
        {
            actual += 1
            val view2 = bind!!.root
            setContentView(view2)
        }
        else
        {
            actual -= 1
            val view2 = bindingInicial.root
            setContentView(view2)
        }


    }

    override fun render(state: UserState) {
        with(state) {

            when {
                state.otraView -> {
                    val intent = Intent(this@Register, ListaServicios::class.java)
                    startActivity(intent)

                }
                state.pedir -> {
                    avanzarPantalla(clientBinder, true)
                    clientBinder.Pasos.setLabels(pasos).setBarColorIndicator(Color.BLACK).setLabelColorIndicator(Color.parseColor("#0ca450")).setCompletedPosition(actual).drawView()
                    clientBinder.Pasos.completedPosition = actual
                }
                state.registrarCliente -> {

                    if (helper.isConnected(this@Register))
                    {
                        lifecycleScope.launch(Dispatchers.Main) {
                            val userInfo = createUserForRegister()
                            val service = RestApiService()
                            val response= service.addUser(userInfo) {

                            }
                            val message=response.awaitResponse()
                            if (message.isSuccessful) {
                                val intent = Intent(this@Register, ListaServicios::class.java)
                                actual += 1
                                val view2 = completado.root
                                setContentView(view2)
                                completado.Pasos.setLabels(pasos).setBarColorIndicator(Color.BLACK).setLabelColorIndicator(Color.parseColor("#0ca450")).setCompletedPosition(actual).drawView()
                                completado.Pasos.completedPosition = actual
                            } else {

                                helper.showWrongRegisterDialog("Either the username or email address are already in use", this@Register)
                            }
                        }

                    }
                    else {

                        helper.showNoConnectionDialog(this@Register)
                    }
                }
                state.proveer -> {
                    avanzarPantalla(workerBinder, true)
                    workerBinder.Pasos.setLabels(pasos).setBarColorIndicator(Color.BLACK).setLabelColorIndicator(Color.parseColor("#0ca450")).setCompletedPosition(actual).drawView()
                    workerBinder.Pasos.completedPosition = actual

                }
                state.volver -> {
                    avanzarPantalla(bindingInicial, false)
                }
                state.login -> {
                    if (helper.isConnected(this@Register))
                    {

                            lifecycleScope.launch {
                                val userInfo = createUserForLogin()
                                val service = RestApiService()
                                val response= service.loginUser(userInfo) {
                                }
                                try {
                                    val message=response.clone().awaitResponse()
                                    val worker= encontrarWorker(userInfo.username)
                                    if (!worker.name.equals(""))
                                    {
                                        val intent = Intent(this@Register, ListaServicios::class.java)
                                        loginWorker(worker)
                                        startActivity(intent)
                                    }
                                    else
                                    {
                                        val user=encontrarUsuario(userInfo.username)

                                        if (message.isSuccessful) {
                                            loginCliente(user)
                                            val intent = Intent(this@Register, ListaServicios::class.java)
                                            startActivity(intent)

                                        } else {
                                            helper.showWrongLoginDialog(this@Register)
                                        }
                                    }
                                }catch (e:Exception)
                                {
                                    helper.showErrorDialog("Connection Problem","Something went wrong, please try again",this@Register)
                                }
                    }
                    }
                    else
                    {
                        helper.showNoConnectionDialog(this@Register)
                    }
                }
                state.registrarWorker -> {
                    if (helper.isConnected(this@Register))
                    {
                        lifecycleScope.launch(Dispatchers.Main) {
                            val userInfo = createWorkerForRegister()
                            val service = RestApiService()
                            val response= service.addWorker(userInfo) {
                            
                            }
                            val message=response.awaitResponse()
                            if (message.isSuccessful) {

                                loginWorker(userInfo)
                                actual += 1
                                val view2 = completado.root
                                setContentView(view2)
                                completado.Pasos.setLabels(pasos).setBarColorIndicator(Color.BLACK).setLabelColorIndicator(Color.parseColor("#0ca450")).setCompletedPosition(actual).drawView()
                                completado.Pasos.completedPosition = actual
                            } else {
                                   
                                helper.showWrongRegisterDialog("Either the username or email address are already in use", this@Register)
                            }
                        }

                    }
                    else {

                        helper.showNoConnectionDialog(this@Register)
                    }
                }
                state.enterLogin -> {
                    avanzarPantalla(loginBind, true)
                }
            }
            if (errorMessage != null) {
                Toast.makeText(this@Register, errorMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }
    suspend fun createUserForRegister():Usuario
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION
                    ),
                    99
            )
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    99
            )
        }
        var locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var locationListener = helperLocation()
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0.toFloat(), locationListener)
        var ubicacion= locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
        val lat = String.format("%.5f", ubicacion!!.latitude).replace(",",".")
        val lon = String.format("%.5f", ubicacion!!.longitude).replace(",",".")
        var user=Usuario(
            id="",
            username = bindingInicial.username.text.toString(),
            email = bindingInicial.editTextTextEmailAddress.text.toString(),
            password = bindingInicial.editTextTextPassword.text.toString(),
            address_desc = clientBinder.Direccion.text.toString(),
            address_lat = lat.toDouble(),
            address_lon = lon.toDouble(),
            id_number = clientBinder.cedula.text.toString(),
            name = bindingInicial.editTextTextPersonName2.text.toString(),
            phone_number = clientBinder.Telefono.text.toString(),
            is_worker = false,
            birth_date = bindingInicial.birthDate.text.toString())
        loginCliente(user)
        return user
    }
    fun createUserForLogin():Usuario
    {

        return Usuario(
                id="",
                username = loginBind.editTextTextEmailAddress.text.toString(),
                email = "",
                password = loginBind.editTextTextPassword.text.toString(),
                address_desc = "",
                address_lat = 0.0,
                address_lon = 0.0, id_number = "",
                name = "",
                phone_number = "",
                is_worker = false,
                birth_date = ""
        )
    }
    fun createWorkerForRegister():Worker{
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION
                    ),
                    99
            )
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    99
            )
        }
        var locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var locationListener = helperLocation()
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0.toFloat(), locationListener)
        var ubicacion= locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        val lat = String.format("%.5f", ubicacion!!.latitude).replace(",",".")
        val lon = String.format("%.5f", ubicacion!!.longitude).replace(",",".")
        var worker=Worker(
            id="",
            id_number = workerBinder.cedula.text.toString(),
            email = bindingInicial.editTextTextEmailAddress.text.toString(),
            username = bindingInicial.username.text.toString(),
            password = bindingInicial.editTextTextPassword.text.toString(),
            name = bindingInicial.editTextTextPersonName2.text.toString(),
            phone_number = workerBinder.Telefono.text.toString(),
            rating = 5.0,
            service_radius = workerBinder.radio.text.toString().toInt(),
            price_per_hour = workerBinder.precio.text.toString().toDouble(),
            address_lat = lat.toDouble(),
            address_lon = lon.toDouble(),
            address_desc = workerBinder.Direccion.text.toString(),
            profile_picture = "http://growplumbing.com/wp-contre/uploads/2016/04/pro-plumber.jpg",
            category = arrayOf(workerBinder.categoria.selectedItem.toString()),
            skill = arrayOf("Punctuality"),
            previous_jobs_img  = arrayOf("http://growplumbing.com/wp-contre/uploads/2016/04/pro-plumber.jpg"),
            is_worker = "True",
            birth_date =  bindingInicial.birthDate.text.toString())
        loginWorker(worker)

            return worker
    }
    suspend fun encontrarUsuario(nombre: String): Usuario {
            val service = RestApiService()
            val lista=service.getUsers(){}
            lista.awaitResponse().body()?.forEach {
                if (it.username==nombre) {
                    if (!it.is_worker) {
                        return it
                    }

                }
            }
            return Usuario("","", "", "", "", "", 0.0, 0.0, "", "", false,"")

    }
    suspend fun encontrarWorker(nombre: String): Worker {
        val service = RestApiService()
        val lista=service.getWorkers(){  }
        lista.forEach {
            if (it.username==nombre) {
                println(it)
                return it
        }}
        return Worker("","", "", "", "", "", "", 0.0, 0, 0.0, 0.0, 0.0, "","", arrayOf(""),arrayOf(""),arrayOf(""), "True","")

    }

    fun loginCliente(user:Usuario) {
        val sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply{
            remove("username")
            remove("name")
            remove("email")
            remove("number")
            remove("id")
            remove("is")
            remove("myId")
            putString("username", user.username)
            putString("name", user.name)
            putString("email", user.email)
            putString("number", user.phone_number)
            putString("id", user.id_number)
            putString("myId", user.id)
            putBoolean("is", false)
            putString("lat", user.address_lat.toString())
            putString("lon", user.address_lon.toString())

        }.apply()
    }
    fun loginWorker(worker:Worker)
    {
        val sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        var categories = ""
        if(worker.category!=null && !worker.category.isEmpty()){
            for(i in worker.category){
                println(i)
                categories = categories + i + ", "
            }
        }
        else{
            categories = "No categories selected"
        }
        editor.apply{
            remove("username")
            remove("name")
            remove("email")
            remove("number")
            remove("id")
            remove("is")
            remove("rating")
            remove("price")
            remove("category")
            remove("ratio")

            putString("username", worker.username)
            putString("name", worker.name)
            putString("email", worker.email)
            putString("number", worker.phone_number)
            putString("id", worker.id_number)
            putBoolean("is", true)
            putString("rating", worker.rating.toString())
            putString("price", worker.price_per_hour.toString())
            putString("category", categories)
            putString("ratio", worker.service_radius.toString())
            putString("lat", worker.address_lat.toString())
            putString("lon", worker.address_lon.toString())
        }.apply()
    }
    public class helperLocation: LocationListener{

        override fun onLocationChanged(location: Location) {
            var latitude: Double = location.getLatitude();
            var longitude:Double = location.getLongitude();
        }

    }
    private fun dateSelector() {
        val newFragment = SelecterFragment.newInstance(DatePickerDialog.OnDateSetListener { _, year, month, day ->
            // +1 because January is zero
            val selectedDate =year.toString() + "-" + (month + 1)+ "-" + day
            birthdate.setText(selectedDate)
        })

        newFragment.show(supportFragmentManager, "datePicker")
    }



}