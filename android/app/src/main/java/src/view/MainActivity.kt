package src.view

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import com.example.services_app.R
import kotlinx.coroutines.launch
import mvi.BaseView
import org.koin.androidx.viewmodel.ext.android.viewModel
import src.intent.WorkerIntent
import src.state.WorkerState
import src.viewmodel.WorkerViewModel
import kotlinx.android.synthetic.main.activity_main.*
import src.adapter.WorkerAdapter
import src.model.Worker


class MainActivity : AppCompatActivity(), BaseView<WorkerState>{
    private val mAdapter = WorkerAdapter()
    private val mViewModel by viewModel<WorkerViewModel>()
    private val helper= Helper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView.adapter = mAdapter

        mViewModel.state.observe(this, Observer {
            render(it)
        })

        lifecycleScope.launch {
            if(helper.isConnected(this@MainActivity))
            {
                mViewModel.intents.send(WorkerIntent.FetchWorkers)
            }
            else{
                helper.showNoConnectionDialog(this@MainActivity)
            }
        }
        btnMap.setOnClickListener {
            lifecycleScope.launch {
                if(helper.isConnected(this@MainActivity))
                {
                    mViewModel.intents.send(WorkerIntent.GetMap)
                }
                else{
                    helper.showNoConnectionDialog(this@MainActivity)
                }
            }
        }

        btnRefresh.setOnClickListener {
            lifecycleScope.launch {
                mViewModel.intents.send(WorkerIntent.RefreshWorkers)
            }
        }

    }
    override fun render(state: WorkerState) {
        with(state) {
            progressBar.isVisible = isLoading
            btnRefresh.isEnabled = !isLoading
            var parcelableWorkers = arrayListOf<String>()
            for (i in workers!!) {
                if (i.category != null) {
                    parcelableWorkers.add("" + i.address_lat + "/" + i.address_lon + "/" + i.name + "/" + i.category[0] + "/" + i.service_radius)
                }
                else{
                    parcelableWorkers.add("" + i.address_lat + "/" + i.address_lon + "/" + i.name + "/" + "No category" + "/" + i.service_radius)
                }
            }
            if(state.isMap){

                val intent = Intent(this@MainActivity, Map::class.java)
                intent.putExtra("workersList", parcelableWorkers)
                startActivity(intent)
            }
            var newWorkers = mutableListOf<Worker>()
            var filter = intent.getStringExtra("filter")
            if( filter.equals("no")) newWorkers = workers.toMutableList()
            else{
                for(i in workers){
                    if(i.category != null){
                        for(j in i.category){
                            if(j.equals(filter)){
                                println("siu")
                                newWorkers.add(i)
                            }
                        }
                    }

                }
            }
            mAdapter.submitList(newWorkers)
            if (errorMessage != null) {
                if(helper.isConnected(this@MainActivity))
                {
                    Toast.makeText(this@MainActivity, errorMessage, Toast.LENGTH_SHORT).show()
                }
                else{
                    helper.showNoConnectionDialog(this@MainActivity)
                }

            }
        }
    }
}
