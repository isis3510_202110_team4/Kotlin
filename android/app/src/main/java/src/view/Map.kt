package src.view

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.services_app.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions


class Map : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener {

    private lateinit var map:GoogleMap

    companion object{
        const val REQUEST_CODE_LOCATION = 0
    }
    private val helper= Helper()
    var workers : ArrayList<String> ?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.map)
        workers = intent.extras?.getStringArrayList("workersList")
        createFragment()
    }

    private fun createFragment() {
        val mapFragment : SupportMapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        var locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        var locationListener = Register.helperLocation()
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestLocationPermission()
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0.toFloat(), locationListener)
        var location= locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        val UserLat = String.format("%.5f", location!!.latitude).replace(",",".").toDouble()
        val UserLon = String.format("%.5f", location!!.longitude).replace(",",".").toDouble()
        val results = FloatArray(1)
        if(workers != null){
            for(i in workers!!){
                var location = i.split("/")
                var workerLat = location[0].toDouble()
                var workerLon = location[1].toDouble()
                var name = location[2].toString()
                var radio = location[4].toDouble()
                android.location.Location.distanceBetween(UserLat, UserLon, workerLat, workerLon, results)
                val distance = results[0].toDouble()
                if(distance < radio*100) {
                createMarker(workerLat, workerLon, name, location[3].toString(), radio)
            }
            }
        }

        map.setOnMyLocationButtonClickListener(this)
        map.setOnMyLocationClickListener(this)
        enableLocation()
    }

    private fun createMarker(latitud: Double, longitud: Double, nombre: String, categoria: String, radio: Double) {
        val coordinates = LatLng(latitud, longitud)
        val marker : MarkerOptions = MarkerOptions().position(coordinates).title("$nombre, $categoria")
            map.addMarker(marker)
            val circle = map.addCircle(
                CircleOptions()
                    .center(coordinates)
                    .radius(radio * 100)
                    .strokeColor(Color.RED)
            )
        map.animateCamera(
                CameraUpdateFactory.newLatLngZoom(coordinates, 18f),
                4000,
                null
        )
    }

    private fun isLocationPermissionGranted() =
        ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED

    private fun enableLocation(){
        if(!::map.isInitialized) return
        if(isLocationPermissionGranted()){
            map.isMyLocationEnabled = true
        }
        else{
            requestLocationPermission()
        }
    }

    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(this, "To view the map you must allow access to your location", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION)
        }
    }

    override fun onRequestPermissionsResult (
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode){
            REQUEST_CODE_LOCATION -> if(grantResults.isNotEmpty() && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                map.isMyLocationEnabled = true
            }
            else {
                Toast.makeText(this, "To view the map you must allow access to your location", Toast.LENGTH_SHORT).show()
            }
            else -> {}
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        if(!::map.isInitialized) return
        if(!isLocationPermissionGranted()){
            map.isMyLocationEnabled = false
            Toast.makeText(this, "To use this functionality you must allow access to your location", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onMyLocationButtonClick(): Boolean {
        return false
    }

    override fun onMyLocationClick(p0: Location) {

    }
}