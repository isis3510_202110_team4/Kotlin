package src.view

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.services_app.R
import java.lang.Exception

class WorkerProfile : AppCompatActivity() {
    private val helper= Helper()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_worker_profile)
        var image: ImageView = findViewById(R.id.userImage)
        var profile: TextView = findViewById(R.id.Profile)
        var user: TextView = findViewById(R.id.usernameS)
        var name: TextView = findViewById(R.id.nameS)
        var email: TextView = findViewById(R.id.emailS)
        var number: TextView = findViewById(R.id.numberS)
        var id: TextView = findViewById(R.id.idS)
        var rating: TextView = findViewById(R.id.ratingS)
        var category: TextView = findViewById(R.id.categoryS)
        var ratio: TextView = findViewById(R.id.ratioS)
        var price: TextView = findViewById(R.id.priceS)
        var boton: Button= findViewById(R.id.buttonCreate)
        var u:String?=null
        var n:String?=null
        var e:String?=null
        var nu:String?=null
        var i:String?=null
        var r:String?=null
        var c:String?=null
        var rtg:String?=null
        var p:String?=null
        var a:String?=null
        var workerId=intent.getStringExtra("id_db")

        boton.setOnClickListener {  val intent = Intent(
                this@WorkerProfile,
                CreateService::class.java)
            intent.putExtra("name", u)
            intent.putExtra("category", c)
            intent.putExtra("price", p)
            intent.putExtra("id_db", workerId)
            startActivity(intent)
        }
        val sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE)

        var solicitar= intent.getBooleanExtra("solicitate",false)


try{
    profile.setText("Worker Information")
     u= intent.getStringExtra("username")
     n= intent.getStringExtra("name")
     e= intent.getStringExtra("email")
     nu= intent.getStringExtra("number")
     i= intent.getStringExtra("id")
     r = intent.getStringExtra("ratio")
     c = intent.getStringExtra("category")
     rtg= intent.getStringExtra("rating")
     p= intent.getStringExtra("price")
    a = intent.getStringExtra("profile_picture")
    Glide.with(this).load(a).placeholder(R.drawable.baseline_account_circle_black_24).error(R.drawable.baseline_cloud_off_black_24dp).circleCrop().into(image)
}catch (e:Exception)
{

}
        if (!solicitar) {
            boton.setVisibility(View.GONE)
            profile.setText("Your Profile")
              u= sharedPreferences.getString("username",null);
              n= sharedPreferences.getString("name",null);
              e= sharedPreferences.getString("email",null);
              nu= sharedPreferences.getString("number",null);
              i= sharedPreferences.getString("id",null);
              r= sharedPreferences.getString("ratio",null);
              c= sharedPreferences.getString("category",null);
             rtg= sharedPreferences.getString("rating","5.0")
             p= sharedPreferences.getString("price","0.0")


        }
        if (u!=null)
            user.setText(u)
        else
            user.setText("invalid username")
        if (n!=null)
            name.setText(n)
        else
            name.setText("invalid name")
        if (e!=null)
            email.setText(e)
        else
            email.setText("invalid email")
        if (nu!=null)
            number.setText(nu)
        else
            number.setText("invalid phone number")
        if (i!=null)
            id.setText(i)
        else
            id.setText("invalid id")
        if (r!=null)
            ratio.setText(r.toString()+" Km")
        else
            ratio.setText("no ratio decided")
        if (c!=null && !c.isEmpty())
            category.setText(c)

        else
            category.setText("no category selected")
        if (rtg!=null && rtg!="null")
            rating.setText(rtg.toString())
        else
            rating.setText("5")
        if (p!=null)
            price.setText(p.toString()+" $"+ "USD")
        else
            price.setText("no price decided")

    }
}