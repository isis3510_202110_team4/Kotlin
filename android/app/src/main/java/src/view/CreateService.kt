package src.view

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.example.services_app.R
import kotlinx.coroutines.launch
import retrofit2.awaitResponse
import src.back.RestApiService
import src.model.Service
import java.util.*
import java.time.LocalDateTime

class CreateService : AppCompatActivity() {
     lateinit  var desc: EditText
    lateinit  var payment: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_service)
        var name: TextView = findViewById(R.id.nameS)
        var category: TextView = findViewById(R.id.categoryS)
        desc = findViewById(R.id.descriptionS)
        payment = findViewById(R.id.paymentS)
        var date: TextView = findViewById(R.id.date)
        var dateS: TextView = findViewById(R.id.dateS)
        var price: TextView = findViewById(R.id.priceS)
        var stateS: TextView = findViewById(R.id.stateS)
        var state: TextView = findViewById(R.id.state)
        var create: Button = findViewById(R.id.buttonCreate)
        var end: Button = findViewById(R.id.buttonEnd)
        var cancel: Button = findViewById(R.id.buttonCancel)
        cancel.setOnClickListener { finish() }
        create.setOnClickListener {
            lifecycleScope.launch {
                try {
                    val service = RestApiService()
                    val serviceInfo= generateService()
                    val response= service.createService(serviceInfo) {

                    }
                    val message=response.awaitResponse()
                    println(message)
                    if (message.isSuccessful) {
                        Toast.makeText(this@CreateService, "Service Created", Toast.LENGTH_SHORT).show()
                        val intent = Intent(
                            this@CreateService,
                            ListaServicios::class.java)
                        startActivity(intent)
                    }
                    else{
                        println(message.errorBody())
                    }
                }catch (e: Exception ){
                    println(e.message)
                    println(e.stackTrace)
                }


        }
        }
        val n= intent.getStringExtra("name");
        var c= intent.getStringExtra("category");
        val p= intent.getStringExtra("price")
        val inProgress=intent.getBooleanExtra("in_progress",false)
        if (!inProgress)
        {
            end.setVisibility(View.GONE)
            state.setVisibility(View.GONE)
            stateS.setVisibility(View.GONE)
            date.setVisibility(View.GONE)
            dateS.setVisibility(View.GONE)
        }
        if (n!=null)
            name.setText(n)
        else
            name.setText("invalid name")
        if (c!=null)
            category.setText(c)
        else
            category.setText("no category selected")
        if (p!=null)
            price.setText(p.toString()+" $"+ "USD")
        else
            price.setText("no price decided")
    }
    fun generateService():Service
    {
        val sharedPreferences= getSharedPreferences("userInfo",Context.MODE_PRIVATE)
        var id=sharedPreferences.getString("myId","")
        var lat=sharedPreferences.getString("lat","")
        var lon=sharedPreferences.getString("lon","")
        val worker=intent.getStringExtra("id_db")
        println(worker)
        return Service("",id,worker ,
            intent.getStringExtra("category"),"5",true,desc.text.toString(),
            LocalDateTime.now().toString(),null,
                intent.getStringExtra("price")?.toDouble(),false,"CH",
            lon,lat,"SH"
        )
    }

    }
