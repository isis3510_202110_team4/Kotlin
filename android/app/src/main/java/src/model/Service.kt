package src.model

data class Service (val id: String?,
                    val client: String?,
                    val worker: String?,
                    val category: String?,
                    var rating: String?,
                    val in_progress: Boolean,
                    val desc: String?,
                    val date_scheduled: String?,
                    val date_finished: String?,
                    val price: Double?,
                    val cancelled: Boolean,
                    val payment_method: String?,
                    val address_lon: String?,
                    val address_lat: String?,
                    val source: String?)


