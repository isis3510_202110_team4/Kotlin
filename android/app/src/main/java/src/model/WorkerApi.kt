package src.model

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

interface WorkerApi {
    @Headers("Content-Type: application/json")
    @GET("api/users/worker/")
    suspend fun getWorkers() : List<Worker>
    @POST("api/users/register/")
    fun registerWorker(@Body userData: Worker) : Call<Worker>
}