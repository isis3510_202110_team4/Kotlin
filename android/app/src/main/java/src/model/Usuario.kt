package src.model

import com.squareup.moshi.Json


data class Usuario(
    val id:String,
    val username: String,
    val email: String,
    val password: String,
    val name: String,
    val address_desc: String,
    val address_lon: Double,
    val address_lat: Double,
    val phone_number: String,
    val id_number: String,
    val is_worker: Boolean,
    val birth_date: String
)