package src.model

import retrofit2.Call
import retrofit2.http.*

interface UsersApi {

    @Headers("Content-Type: application/json")
    @POST("api/users/register/")
     fun registerUser(@Body userData: Usuario) : Call<Usuario>
    @POST("api/users/login/")
    fun loginUser(@Body userData: Usuario) : Call<Usuario>
    @GET("api/users/client/")
    fun getUsers() : Call<List<Usuario>>
    @GET("api/users/client/detail/{id}/favorite_category")
    fun getFavoriteCategory(@Path("id") id:String): Call<Categoria>
    @GET("/api/users/worker/best_by_category")
    fun getBestWorkerPerCategory(): Call<List<Worker>>
    @POST("api/service/")
    fun createService(@Body serviceData: Service) : Call<Service>
}