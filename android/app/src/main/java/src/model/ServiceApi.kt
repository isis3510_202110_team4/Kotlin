package src.model

import retrofit2.Call
import retrofit2.http.*

interface ServiceApi {
    @Headers("Content-Type: application/json")
    @GET("api/users/client/detail/{id}/services/")
    suspend fun getServices(@Path("id") id:String) : List<Service>
    @GET("api/service/")
    suspend fun getAllServices() : List<Service>
}