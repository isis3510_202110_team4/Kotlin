package src.model

import android.os.Parcel
import android.os.Parcelable


data class Worker(
    val id: String?,
    val id_number: String?,
    val email: String?,
    val username: String?,
    val password: String?,
    val name: String?,
    val phone_number: String?,
    var rating: Double?,
    val service_radius: Int,
    val price_per_hour: Double,
    val address_lon: Double,
    val address_lat: Double,
    val address_desc: String?,
    val profile_picture: String?,
    val category: Array<String>?,
    val skill: Array<String>?,
    val previous_jobs_img: Array<String>?,
    val is_worker: String?,
    val birth_date: String?): Parcelable{
    constructor(source: Parcel) : this(
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readDouble(),
        source.readInt(),
        source.readDouble(),
        source.readDouble(),
        source.readDouble(),
        source.readString(),
        source.readString(),
        arrayOf(),
        arrayOf(),
        arrayOf(),
        source.readString(),
        source.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) = with(parcel){
        writeString(id)
        writeString(id_number)
        writeString(email)
        writeString(username)
        writeString(password)
        writeString(name)
        if (rating != null) {
            writeDouble(rating!!)
        }
        else{writeDouble(5.0)}
        writeInt(service_radius)
        writeDouble(price_per_hour)
        writeDouble(address_lon)
        writeDouble(address_lat)
        writeString(profile_picture)
        writeString(category?.get(0))
        writeString(skill?.get(0))
        writeString(previous_jobs_img?.get(0))
        writeString(address_desc)
        writeString(is_worker)
        writeString(birth_date)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Worker> {
        override fun createFromParcel(parcel: Parcel): Worker {
            return Worker(parcel)
        }

        override fun newArray(size: Int): Array<Worker?> {
            return arrayOfNulls(size)
        }
    }
}