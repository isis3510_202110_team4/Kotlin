package src.intent

import mvi.BaseIntent

sealed class UserIntent : BaseIntent {
    object EnterApp : UserIntent()
    object prestar : UserIntent()
    object solicitar: UserIntent()
    object volver: UserIntent()
    object registrar: UserIntent()
    object registrarWorker: UserIntent()
    object login: UserIntent()
    object enterLogin: UserIntent()
    object goProfile: UserIntent()
    object allServices: UserIntent()
    object recommended: UserIntent()
    object Plumbing: UserIntent()
    object Electricity: UserIntent()
    object Carpentry: UserIntent()
    object Landscaping: UserIntent()
    object Housekeeping: UserIntent()
    object Other: UserIntent()
    object ListaServicios : UserIntent()
}