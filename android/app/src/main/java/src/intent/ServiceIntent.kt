package src.intent

import mvi.BaseIntent

sealed class ServiceIntent : BaseIntent{
    object RefreshServices : ServiceIntent()
    object FetchServices : ServiceIntent()
    object allFalse : ServiceIntent()
}